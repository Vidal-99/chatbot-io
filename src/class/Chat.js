import Message from './Message';

class Chat {
  constructor(bots) {
    this.el = document.getElementById('app');
    this.bots = bots;
    this.messages = Message.loadMessages() || [];
    this.run();
  }

  renderBot(bot) {
    const { name, avatar, count } = bot;
    return `
      <li class="list-group-item d-flex justify-content-between align-items-center">
        <img class="rounded-circle" width="50" src="${avatar}" alt="avatar">
        ${name}
        <span class="badge bg-primary rounded-pill">${count}</span>
      </li>
    `;
  }

  renderListBots() {
    return `
      <ul class="list-group list-group-flush">
        ${this.bots.filter((bot) => bot.name !== 'User').map((bot) => this.renderBot(bot)).join('')}
      </ul>
    `;
  }

  renderMessage(message, bot) {
    const { avatar, name } = bot;
    const messageDate = new Date(message.date); // Convertir la date stockée en tant qu'objet Date

    return `
      <div class="${name !== 'User' ? 'chat-message-left pb-4' : 'chat-message-right pb-4'}">
        <div>
          <img src='${avatar}' class="rounded-circle mr-1" alt="${name}" width="40" height="40">
          <div class="text-muted small text-nowrap mt-2">${messageDate.toLocaleTimeString()}</div>
        </div>
        <div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3">
          <div class="font-weight-bold mb-1">${name}</div>
          ${message.text}
        </div>
      </div>
    `;
  }

  renderChatBot() {
    return this.messages.map((message) => {
      const bot = this.bots.find((b) => b.name === message.bot);
      return this.renderMessage(message, bot);
    }).join('');
  }

  render() {
    return `
      <nav class="navbar bg-body-tertiary bg-green sticky-top">
        <div class="container-fluid">
          <span class="navbar-brand mb-0 h1 text-white">ChatBot.IO</span>
        </div>
      </nav>
      <div class="container-fluid">
        <div class="row">
          <div class="col-3 bg-gray-perso pt-2">
            ${this.renderListBots()}
          </div>
          <div class="col-9">
            <div class="position-relative">
              <div class="chat-messages p-4 max-h" id='chat'>
                ${this.renderChatBot()}
              </div>
            </div>
            <div class="flex-grow-0 py-3 px-4 border-top">
              <div class="input-group">
                <input type="text" class="form-control" id='input-user' placeholder="Écrire ici...">
                <button class="btn btn-success" id='send-button'>Envoyer</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  }

  async addMessage() {
    const inputValue = document.querySelector('#input-user');
    const value = inputValue.value.trim().toLowerCase();
    const actionResponses = {
      help: 'Help',
      heure: 'Heure',
      restau: 'Restau',
      jeux: 'Jeux',
      twitter: 'Twitter',
      football: 'Football',
      gpt: 'AI',
      météo: 'Météo'
    };

    if (value !== '') {
      const userMessage = new Message('User', value);
      this.messages.push(userMessage);

      const words = value.split(': ');
      const action = actionResponses[words[0]];

      if (action) {
        this.bots.forEach((bot_) => {
          if (bot_.actions.includes(action)) {
            let response;
            if (action === 'Météo') {
              response = bot_.getActions(action, words[1]);
            } else if (action === 'Restau') {
              response = bot_.getActions(action, words[1]);
            } else if (action === 'Jeux') {
              response = bot_.getActions(action, words[1]);
            } else if (action === 'Twitter') {
              response = bot_.getActions(action, words[1]);
            } else if (action === 'GPT') {
              response = bot_.getActions(action, words[1]);
            } else {
              response = bot_.getActions(action);
            }

            const responseBot = new Message(bot_.name, response, bot_.count);
            this.messages.push(responseBot);
            inputValue.value = '';

            Message.saveMessages(this.messages);
            this.updateChat();
          }
        });
      } else {
        inputValue.value = '';

        Message.saveMessages(this.messages);
        this.updateChat();
      }
    }
  }

  updateChat() {
    const chatElement = document.getElementById('chat');
    chatElement.innerHTML = this.renderChatBot();
  }

  run() {
    this.el.innerHTML = this.render();

    const sendButton = document.getElementById('send-button');
    const inputValue = document.querySelector('#input-user');
    sendButton.addEventListener('click', () => this.addMessage());
    inputValue.addEventListener('keydown', (e) => {
      if (e.key === 'Enter') {
        e.preventDefault();
        this.addMessage();
      }
    });
  }
}

export default Chat;
