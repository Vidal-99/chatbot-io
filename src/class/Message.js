class Message {
  constructor(bot, text, count) {
    this.bot = bot;
    this.text = text;
    this.count = count;
    this.date = new Date();
  }

  static saveMessages(Conv) {
    localStorage.setItem('chatMessages', JSON.stringify(Conv));
  }

  static loadMessages() {
    const storedMessages = localStorage.getItem('chatMessages');
    if (storedMessages) {
      return JSON.parse(storedMessages);
    }
    return [];
  }
}

export default Message;
