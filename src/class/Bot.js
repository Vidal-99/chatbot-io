class Bot {
  constructor(name, avatar, actions = [], count = 0, messages = [], options = {}) {
    this.name = name;
    this.avatar = avatar;
    this.count = count;
    this.actions = actions;
    this.messages = messages;
    this.options = options;
    this.weatherApiUrl = 'https://api.openweathermap.org/data/2.5/weather';
    this.weatherApiKey = '36189af79b33a16fe949a5b781f9fc68';
    this.RestauKey = 'SgjvJnYDNCCyAbCLXd7Cy_FMd6Lb2Al6skEhNz5Y9el1GBlq4EMdxRvCa7Us1dWwMlVpcWzMyFS50RrSqc7nRrMi9NXTPXSizc5pI8wPvMnKaVRiildGMIrAv28xZXYx';
    this.rawgApiKey = '2e696e1a2793430980a554ecc693dbc3';
  }

  addMessage(message) {
    this.messages.push(message);
    this.addCount();
  }

  addCount() {
    this.count += 1;
  }

  async getWeather(city) {
    if (!city) {
      return 'Veuillez spécifier une ville pour obtenir la météo.';
    }

    try {
      const params = {
        q: city,
        appid: this.weatherApiKey
      };

      const response = await fetch(`${this.weatherApiUrl}?${new URLSearchParams(params)}`);
      const data = await response.json();

      if (data.cod === 200) {
        const weatherDescription = data.weather[0].description;
        const temperature = data.main.temp;
        const cityName = data.name;
        this.addCount();
        return `Météo à ${cityName}: ${weatherDescription}, Température: ${temperature}°C`;
      }
      return `Erreur : ${data.message}`;
    } catch (error) {
      return `Erreur : ${error.message}`;
    }
  }

  async getTwitterInfo(username) {
    const url = `https://twitter154.p.rapidapi.com/user/details?username=${username}&user_id=96479162`;
    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': 'd515bf0996msh5a3cf743988bc97p118931jsn633a221c2e56',
        'X-RapidAPI-Host': 'twitter154.p.rapidapi.com'
      }
    };

    try {
      const response = await fetch(url, options);
      const result = await response.text();
      return result.location;
    } catch (error) {
      throw new Error(error);
    }
  }

  async getVideoGameInfo(gameName) {
    if (!gameName) {
      return 'Veuillez spécifier un nom de jeu pour obtenir les infos.';
    }
    try {
      const response = await fetch(`https://api.rawg.io/api/games?search=${gameName}`, {
        headers: {
          key: this.rawgApiKey
        }
      });

      if (response.ok) {
        const data = await response.json();
        if (data.results && data.results.length > 0) {
          const firstGame = data.results[0];
          const description = firstGame.description_raw;
          return description;
        }
        return 'Aucune information trouvée pour ce jeu.';
      }
    } catch (error) {
      throw new Error('Une erreur s\'est produite lors de la récupération des infos jeux :', error);
    }
    return 'Une erreur s\'est produite lors de la récupération des infos jeux.';
  }

  async getNearbyRestaurants(loca) {
    if (!loca) {
      return 'Veuillez spécifier une ville pour obtenir les restau';
    }
    const params = {
      term: 'restaurant',
      location: loca,
      sort_by: 'best_match'
    };

    try {
      const response = await fetch(`https://api.yelp.com/v3/businesses/search?term=${params.term}&location=${params.location}&sort_by=best_match`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${this.RestauKey}`
        }
      });

      if (!response.ok) {
        throw new Error('Échec de la requête HTTP');
      }

      const data = await response.json();
      const restaurants = data.businesses.name;

      return restaurants;
    } catch (error) {
      throw new Error('Une erreur s\'est produite lors de la récupération des restaurants :', error);
    }
  }

  async fetchFootData() {
    const url = 'https://api-football-beta.p.rapidapi.com/timezone';
    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': 'd515bf0996msh5a3cf743988bc97p118931jsn633a221c2e56',
        'X-RapidAPI-Host': 'api-football-beta.p.rapidapi.com'
      }
    };

    try {
      const response = await fetch(url, options);
      const result = await response.text();
      return result;
    } catch (error) {
      throw new Error(error);
    }
  }

  async fetchGNCData() {
    try {
      const response = await fetch('', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      });

      if (!response.ok) {
        throw new Error('Échec de la requête HTTP');
      }

      const data = await response.json();
      this.addCount();
      return data;
    } catch (error) {
      throw new Error('Une erreur s\'est produite lors de la récupération des données:', error);
    }
  }

  getHelp(action) {
    this.addCount();
    return `Je peux faire ca comme action : ${action.map((m) => ` ${m} `)}`;
  }

  getHour() {
    const hour = new Date();
    this.addCount();
    return `il est ${hour.toLocaleTimeString()} à Paris`;
  }

  async actionOpenAI(question) {
    const url = 'https://chatgpt-api8.p.rapidapi.com/';
    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        'X-RapidAPI-Key': 'd515bf0996msh5a3cf743988bc97p118931jsn633a221c2e56',
        'X-RapidAPI-Host': 'chatgpt-api8.p.rapidapi.com'
      },
      body: [
        {
          content: `'${question}'`,
          role: 'user'
        }
      ]
    };

    try {
      const response = await fetch(url, options);
      const result = await response.text();
      return result.text;
    } catch (error) {
      throw new Error(error);
    }
  }

  getActions(action, option) {
    switch (action) {
      case 'Météo':
        if (option) {
          return this.getWeather(option);
        }
        return 'Veuillez spécifier une ville pour obtenir la météo.';
      case 'Help':
        return this.getHelp(this.actions);
      case 'Heure':
        return this.getHour();
      case 'Restau':
        return this.getNearbyRestaurants(option);
      case 'Jeux':
        return this.getVideoGameInfo(option);
      case 'Twitter':
        return this.getTwitterInfo(option);
      case 'Football':
        return this.fetchFootData();
      case 'GNC':
        return this.fetchGNCData();
      case 'GPT':
        return this.actionOpenAI(option);
      default:
        return 'Action non reconnue';
    }
  }
}

export default Bot;
