import Chat from './class/Chat';
import Bot from './class/Bot';
import './index.scss';

const bot0 = new Bot('User', 'https://static.tvtropes.org/pmwiki/pub/images/got_tyrion_lannister.png', [], 0, [], '');
const bot1 = new Bot('spideman', 'https://i.pinimg.com/1200x/f6/aa/24/f6aa2407d3ca6532e0304d6cd0e9291d.jpg', ['Météo', 'Restau', 'Help'], 0, [], '');
const bot2 = new Bot('superman', 'https://avatarfiles.alphacoders.com/252/252153.jpg', ['Heure', 'GPT', 'Twitter', 'Help'], 0, [], '');
const bot3 = new Bot('wonder woman', 'https://cdna.artstation.com/p/assets/images/images/000/993/178/large/dyana-wang-avatar.jpg?1437733741', ['Jeux', 'Football', 'Help'], 0, [], '');

const bots = [bot0, bot1, bot2, bot3];
const app = new Chat(bots);

app.run();
